package org.jay.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.jay.dto.Address;
import org.jay.dto.FourWheeler;
import org.jay.dto.TwoWheeler;
import org.jay.dto.UserDetails;
import org.jay.dto.Vehicle;

public class HibernateTest {

	public static void main(String[] args) {
		SessionFactory sessFactory = null;
		Session session = null;
		try {
			UserDetails user = new UserDetails();
			Address address1 = new Address();
			address1.setStreet("Street name");
			address1.setCity("city name");
			address1.setPincode("pincocde");
			address1.setState("state name");

			Address address2 = new Address();
			address2.setStreet("office Street name");
			address2.setCity("office city name");
			address2.setPincode("office pincocde");
			address2.setState("office state name");

			List<Address> addresses = new ArrayList<Address>();
			addresses.add(address1);
			addresses.add(address2);

			Vehicle vehicle = new Vehicle();
			vehicle.setVehicleName("Car");
			vehicle.getUser().add(user);
			
			TwoWheeler twoWheeler=new TwoWheeler();
			twoWheeler.setVehicleName("splendor");
			twoWheeler.setSteeringHandle("bike");
			twoWheeler.getUser().add(user);
			
			FourWheeler fourWheeler=new FourWheeler();
			fourWheeler.setVehicleName("Car");
			fourWheeler.setSteeringWheel("porsch");
			fourWheeler.getUser().add(user);
			
			user.setListOfAddress(addresses);
			user.setUserName("First user");
			user.setDate(new Date());
			user.setDescription("Description of user goes here");
			user.getVehicle().add(vehicle);
			user.getVehicle().add(twoWheeler);
			user.getVehicle().add(fourWheeler);
			

			

			sessFactory = new Configuration().configure().buildSessionFactory();
			session = sessFactory.openSession();
			session.beginTransaction();
			session.save(user);
			
		
			session.getTransaction().commit();
			session.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
			sessFactory.close();
		}

	}

}
